function ajustar(){
    var altoVentana   = $(window).height();
    var altoCabecera  = $('#cabecera').height();

    if($(window).width() > 768){
        $('#capa-menu-movil').show();
        $('#contenido.home #slide').css('height', altoVentana - altoCabecera-20);
    } else {
        $('#capa-menu-movil').hide();
        $('#contenido.home #slide').css('height', altoVentana - altoCabecera);
    }
}


$(window).resize(function() {
    ajustar();
});

$(window).load(function() {
    ajustar();
    $( ".datepicker" ).datepicker();
    $('#menu li a.tiene-submenu').click(
        function(e) {
            var destino = $(this).attr('data-target');
            $(destino).fadeToggle('fast');
            e.preventDefault();
        }
    );
    $('#control-menu-movil').click(
        function(e) {
            $('#capa-menu-movil').fadeToggle('fast');
            e.preventDefault();
        }
    );
});

$(window).scroll(function() {

});